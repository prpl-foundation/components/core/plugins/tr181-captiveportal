#!/bin/sh

. /usr/lib/amx/scripts/amx_init_functions.sh

name="tr181-captiveportal"
datamodel_root="CaptivePortal"

case $1 in
    boot)
        if [[ -e "/etc/amx/tr181-captiveportal/webpage/lighttpd.conf" ]]; then
            lighttpd -f "/etc/amx/tr181-captiveportal/webpage/lighttpd.conf"
        fi
        process_boot ${name} -D
        ;;
    start)
        if [[ -e "/etc/amx/tr181-captiveportal/webpage/lighttpd.conf" ]]; then
            lighttpd -f "/etc/amx/tr181-captiveportal/webpage/lighttpd.conf"
        fi
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root}
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart]"
        ;;
esac
