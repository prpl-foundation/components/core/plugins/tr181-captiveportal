# Example files

When the WAN connection is down, we want to display a proper status page, but for testing purposes we already want to be able to display a basic page. This folder contains a `lighttpd.conf` file, an `index.html` file and 2 `.svg` images to host a webpage on a local port of the router. When the config option `CONFIG_SAH_AMX_TR181_CAPTIVEPORTAL_EXAMPLE` is set, the `.html` and `.svg` files will be installed in `/etc/amx/tr181-captiveportal/webpage/`. The plugin will make sure an additional `UserInterface.HTTPAccess.` instance is added to host the page via the WebUI. Alternatively, you can host the webpage yourself using the example `lighttpd.conf` file using the following command:

```bash
lighttpd -f lighttpd.conf
```

Note that when `opennds` is running (i.e. the WAN is down), it will automatically open up the firewall to reach this page. If you want to test whether the webpage is reachable, you should open up this port manually via the HL-API using `ubus-cli` for example

```bash
Firewall.Service.+{Action="Accept",Alias="captive-portal",DestPort=2080,Enable=1,IPVersion=4,Interface="Device.IP.Interface.3.",Protocol="6"}
```

which is equivalent to

```bash
iptables -t filter -A INPUT_Services -p tcp --dport 2080 -j ACCEPT
```

You can inspect the new iptables rule with

```bash
iptables -L INPUT_Services
```

When the WAN connection is down, you should automatically be redirected to this page, which contains a simple button. Pressing the button will authenticate you to the network and redirects you to the local WebUI, which is the only thing that will be reachable when the WAN is down.
