# AMX Captive Portal

[[_TOC_]]

## Introduction

This plugin manages the `CaptivePortal` data model and can set up a captive portal for the configured LAN interfaces (currently limited to 1). When a captive portal is running, a splash page will be displayed to clients that connect to the interface. Traditional captive portals require a user to first login or accept the terms of service before they are allowed to browse the internet. Our first use case is different. We want to display a splash page when the WAN interface goes down to inform the user that he won't be able to access the internet. Support for a traditional captive portal (splash page on connect) can be added later.

## Functionality

The current data model contains the 4 existing TR-181 data model parameters `Enable`, `Status`, `AllowedList` and `URL`. Only `Enable` and `Status` are implemented at the moment. We don't need `AllowedList` yet and we want to be able to specify different URLs for splash pages on different interfaces. That's why we have a `LANInterface[]` multi-instance object instead, which contains the relevant parameters to show a captive portal on a given interface.

To display a captive portal page, we will be using the open source solution `openNDS`. This currently only supports a captive portal on a single interface, however we may want to extend `openNDS` in the future with support for captive portals on multiple interfaces, hence the chosen data model.

There are 2 use cases to show a captive portal:

1. When a LAN client connects to the router. This is the default captive portal functionality.
2. When a LAN client is already connected to the router, but the WAN interface goes down. In this case we want to inform the client that the WAN is down via a splash page.

Our main use case is the second one where we display a status page when the WAN goes down. To do this, we will use this plugin to monitor the WAN interface and when it goes down, we launch `opennds` as a subprocess. It will then display the status page we want. Support for use case 1 can be added later with some adaptions to `openNDS`.

### The State parameter

Our data model contains a `CaptivePortal.State` parameter. This parameter is used to track the state of the `opennds` subprocess.
It starts in state Init and can change to `Running` when the WAN goes down. From here it can go to `Stopping` when the WAN comes back up. Stopping it takes a few seconds and we want to make sure we don't spawn a new subprocess while the previous one is still running. From the `Stopping` stage, we can get a signal `proc:stopped` indicating that the subprocess has stopped and we go to the `Stopped` state. It is also possible that the WAN goes down again before the process has fully stopped, in which case we go to the `Restart` state. In the `Restart` state we wait for the `proc:stopped` signal before we actually restart the process to avoid having it up twice. If the WAN goes up before this signal arrives, we go back to the `Stopping` state. When the state is `Stopped`, it can only go back to `Running` if the WAN goes down again. This is visually displayed in the FSM diagram below.

![fsm-process](doc/fsm-process.png)

### The PortalURLs parameter

When you connect to the WiFi via any modern device, the operating system on that device will automatically check if there is a captive portal present on the network. The operating system does this by sending a HTTP GET request to a well-known URL and checking the response. If the response matches whatever the operating system expects, the device concludes there is no captive portal, because if there was one, it would have never been able to reach the web page (the request would be blocked by the captive portal firewall rules). For example, modern android phones will send a GET to `connectivitycheck.gstatic.com` and redirect the user to the captive portal if they don't get the expected "success" response from this well-known page.

There is one problem with this system. If the WAN is down, the captive portal detection will not work and this is exactly the use case we want to implement. The reason why it doesn't work is because the operating system (or browser) first tries to resolve the well-known URL to an IP address and when the WAN is down, this will fail. Luckily this can quite easily be circumvented by redirecting DNS requests for these well-known URLs to the LAN IP address that is still reachable. As a result the operating system (or browser) will send its HTTP GET to port 80 on the LAN IP address, `opennds` will intercept this request and send some kind of a HTTP response back. Whatever the response is, it won't match the "success" page the operating system is expecting and it will again redirect the user to the captive portal.

The `PortalURLs` parameter can be used to specify which URLs need to be redirected to the LAN IP when the WAN is down. A few examples are listed below:

- For modern android devices: `connectivitycheck.gstatic.com`
- For modern windows devices: `msftconnecttest.com`
- For modern apple devices: `captive.apple.com`
- For firefox browsers: `detectportal.firefox.com`
- For google chrome browsers: `clients3.google.com`

This list is not complete, but it already contains some examples for the most used browsers and operating systems. To make sure it works in all use cases, it might be better to redirect literally everything to the LAN IP, but this is something to investigate later.

## Hosting the splash page

The captive portal splash page is hosted by adding an extra instance to the `UserInterface.HTTPAccess` data model (Alias is `captive`). This instance will ensure the web page is available on the `FASPort` configured in the data model.

## Future improvements

Currently `opennds` reconfigures the firewall on startup and cleans up on exit using iptables commands. This needs to be removed from `opennds`, because we want to reconfigure the firewall using the HL-API. This also means that we can only start `opennds` when the WAN goes down, because otherwise we would immediately show a captive portal on first connect, which is not something we want to enforce.
