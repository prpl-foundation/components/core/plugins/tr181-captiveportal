include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_extra.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_extra.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-captiveportal_mapping.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.d
	$(INSTALL) -D -p -m 0644 odl/defaults.d/* $(DEST)/etc/amx/$(COMPONENT)/defaults.d/
	$(INSTALL) -d -m 0755 $(DEST)$(PROCMONDIR)
	ln -sfr $(DEST)$(INITDIR)/$(COMPONENT) $(DEST)$(PROCMONDIR)/$(COMPONENT)
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)
ifeq ($(CONFIG_SAH_AMX_TR181_CAPTIVEPORTAL_EXAMPLE),y)
	$(INSTALL) -D -p -m 0644 examples/index.html $(DEST)/etc/amx/$(COMPONENT)/webpage/index.html
endif
ifeq ($(CONFIG_SAH_AMX_TR181_CAPTIVEPORTAL_EXAMPLE),y)
	$(INSTALL) -D -p -m 0644 examples/background.svg $(DEST)/etc/amx/$(COMPONENT)/webpage/background.svg
endif
ifeq ($(CONFIG_SAH_AMX_TR181_CAPTIVEPORTAL_EXAMPLE),y)
	$(INSTALL) -D -p -m 0644 examples/sah-logo.svg $(DEST)/etc/amx/$(COMPONENT)/webpage/sah-logo.svg
endif

package: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_extra.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_extra.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-captiveportal_mapping.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.d
	$(INSTALL) -D -p -m 0644 odl/defaults.d/* $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/
	$(INSTALL) -d -m 0755 $(PKGDIR)$(PROCMONDIR)
	rm -f $(PKGDIR)$(PROCMONDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(INITDIR)/$(COMPONENT) $(PKGDIR)$(PROCMONDIR)/$(COMPONENT)
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
ifeq ($(CONFIG_SAH_AMX_TR181_CAPTIVEPORTAL_EXAMPLE),y)
	$(INSTALL) -D -p -m 0644 examples/index.html $(PKGDIR)/etc/amx/$(COMPONENT)/webpage/index.html
endif
ifeq ($(CONFIG_SAH_AMX_TR181_CAPTIVEPORTAL_EXAMPLE),y)
	$(INSTALL) -D -p -m 0644 examples/background.svg $(PKGDIR)/etc/amx/$(COMPONENT)/webpage/background.svg
endif
ifeq ($(CONFIG_SAH_AMX_TR181_CAPTIVEPORTAL_EXAMPLE),y)
	$(INSTALL) -D -p -m 0644 examples/sah-logo.svg $(PKGDIR)/etc/amx/$(COMPONENT)/webpage/sah-logo.svg
endif
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package test