/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>

#include "captive_portal.h"
#include "cp_config.h"
#include "dm_lan_intf.h"

typedef struct _setting_map {
    const char* param_name;
    const char* setting_name;
} setting_map_t;

static void cp_config_set_fixed(amxc_string_t* const config) {
    amxc_string_setf(config, "%s", "config opennds\n");
    amxc_string_appendf(config, "%s", "\toption enabled 1\n");
    amxc_string_appendf(config, "%s", "\toption login_option_enabled '0'\n");
    amxc_string_appendf(config, "%s", "\toption auth_restore '0'\n");
    amxc_string_appendf(config, "%s", "\toption fas_secure_enabled '0'\n");
}

static void cp_config_set_interface(amxc_string_t* const config, amxd_object_t* const object) {
    const char* gw_interface = NULL;
    amxc_string_t* dotted_path = dm_lan_intf_get_dotted(object);
    amxc_var_t* first_param = NULL;

    when_null(dotted_path, exit);

    first_param = netmodel_getFirstParameter(amxc_string_get(dotted_path, 0), "Name", NULL, "down");
    gw_interface = amxc_var_constcast(cstring_t, first_param);
    when_str_empty(gw_interface, exit);

    amxc_string_appendf(config, "\toption gatewayinterface '%s'\n", gw_interface);
exit:
    amxc_var_delete(&first_param);
    amxc_string_delete(&dotted_path);
    return;
}

static int cp_config_write_file(const amxc_string_t* const config) {
    int retval = -1;
    amxo_parser_t* parser = cp_get_parser();
    FILE* file = NULL;
    const char* file_name = GET_CHAR(&parser->config, "cp_cfg_file");

    when_str_empty(file_name, exit);

    file = fopen(file_name, "w+");
    when_null(file, exit);
    when_true(fputs(amxc_string_get(config, 0), file) == EOF, exit);

    retval = 0;
exit:
    if(file != NULL) {
        fclose(file);
    }
    return retval;
}

int cp_config_generate_file(amxd_object_t* const object) {
    int retval = -1;
    amxc_var_t params;
    amxc_string_t config;
    amxc_var_t* setting = NULL;
    setting_map_t mapper[] = {
        {"FASPath", "faspath"},
        {"FASPort", "fasport"},
        {NULL, NULL}
    };

    SAH_TRACEZ_INFO(ME, "Generate config file for opennds");
    amxc_string_init(&config, 0);
    amxc_var_init(&params);

    cp_config_set_fixed(&config);
    cp_config_set_interface(&config, object);

    amxd_object_get_params(object, &params, amxd_dm_access_private);
    for(int i = 0; mapper[i].param_name != NULL; i++) {
        setting = GET_ARG(&params, mapper[i].param_name);

        switch(amxc_var_type_of(setting)) {
        case AMXC_VAR_ID_CSTRING:
            amxc_string_appendf(&config,
                                "\toption %s '%s'\n",
                                mapper[i].setting_name,
                                GET_CHAR(setting, NULL));
            break;
        case AMXC_VAR_ID_BOOL:
            amxc_string_appendf(&config,
                                "\toption %s %s\n",
                                mapper[i].setting_name,
                                GET_BOOL(setting, NULL) ? "1" : "0");
            break;
        case AMXC_VAR_ID_UINT32:
            amxc_string_appendf(&config,
                                "\toption %s '%u'\n",
                                mapper[i].setting_name,
                                GET_UINT32(setting, NULL));
            break;
        default:
            SAH_TRACEZ_ERROR(ME, "Variant type (%d) not supported, skipping...", amxc_var_type_of(setting));
            break;
        }
    }
    retval = cp_config_write_file(&config);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to write config file");
        goto exit;
    }

exit:
    amxc_string_clean(&config);
    amxc_var_clean(&params);
    return retval;
}