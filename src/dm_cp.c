/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "captive_portal.h"
#include "dm_cp.h"
#include "dm_lan_intf.h"
#include "cp_utils.h"
#include "cp_wan.h"
#include "cp_opennds.h"

static int cb_lan_enabled(UNUSED amxd_object_t* start_object,
                          amxd_object_t* matching_object,
                          UNUSED void* priv) {
    dm_lan_added_and_enabled(matching_object);
    return 0;
}

int dm_cp_set_value(amxd_object_t* object, const char* param, const char* value) {
    int retval = -1;
    amxd_dm_t* dm = cp_get_dm();
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_str_empty(param, exit);
    when_str_empty(value, exit);

    if(object == NULL) {
        object = amxd_dm_findf(dm, "CaptivePortal");
    }

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, param, value);
    retval = amxd_trans_apply(&trans, cp_get_dm());

exit:
    amxd_trans_clean(&trans);
    return retval;
}

bool dm_cp_get_enable(void) {
    amxd_object_t* object = amxd_dm_findf(cp_get_dm(), "CaptivePortal.");
    return amxd_object_get_value(bool, object, "Enable", NULL);
}

char* dm_cp_get_state(void) {
    amxd_object_t* object = amxd_dm_findf(cp_get_dm(), "CaptivePortal.");
    return amxd_object_get_value(cstring_t, object, "State", NULL);
}

void _dm_cp_enabled(UNUSED const char* const sig_name,
                    UNUSED const amxc_var_t* const data,
                    UNUSED void* const priv) {
    amxd_object_t* object = amxd_dm_findf(cp_get_dm(), "CaptivePortal.");
    SAH_TRACEZ_INFO(ME, "CaptivePortal global enable is true");
    dm_cp_set_value(NULL, "Status", "Enabled");
    cp_wan_query_open();
    amxd_object_for_all(object, "LANInterface.[Enable==true].", cb_lan_enabled, NULL);
}

void _dm_cp_disabled(UNUSED const char* const sig_name,
                     UNUSED const amxc_var_t* const data,
                     UNUSED void* const priv) {
    amxd_object_t* lan_obj = amxd_dm_findf(cp_get_dm(), "CaptivePortal.LANInterface.1.");
    amxp_proc_ctrl_t* opennds_ctrl = cp_get_proc_ctrl();

    SAH_TRACEZ_INFO(ME, "CaptivePortal global enable is false");
    dm_cp_set_value(NULL, "Status", "Disabled");
    cp_wan_query_close();

    // Make sure we stop opennds, because we won't receive any further netmodel events
    when_null(opennds_ctrl, exit);
    if(amxp_subproc_is_running(opennds_ctrl->proc)) {
        cp_opennds_stop();
        cp_opennds_dns_redirect_stop(lan_obj);
    }

exit:
    return;
}

amxc_string_t* dm_cp_wan_intf_get_dotted(void) {
    amxd_object_t* object = amxd_dm_findf(cp_get_dm(), "CaptivePortal.");
    char* interface = amxd_object_get_value(cstring_t, object, "WANInterface", NULL);
    amxc_string_t* dotted_intf = NULL;

    when_str_empty(interface, exit);
    dotted_intf = cp_utils_add_dot(interface);

exit:
    free(interface);
    return dotted_intf;
}
