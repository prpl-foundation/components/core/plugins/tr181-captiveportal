/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <libgen.h>
#include <string.h>

#include "captive_portal.h"
#include "cp_httpaccess.h"
#include "dm_lan_intf.h"

#define BUFFERSIZE 1024

static int copy_webpage_file(const char* name, UNUSED void* priv) {
    int retval = -1;
    int fd_in = 0;
    int fd_out = 0;
    ssize_t r_chars = 0;
    ssize_t w_chars = 0;
    char buf[BUFFERSIZE];
    char* name_copy = NULL;
    amxo_parser_t* parser = cp_get_parser();
    const char* dst_dir = GET_CHAR(amxo_parser_get_config(parser, "cp_httpaccess_dst"), NULL);
    amxc_string_t dst_path;

    memset(buf, 0, BUFFERSIZE * sizeof(char));
    amxc_string_init(&dst_path, 0);

    when_str_empty(name, exit);

    // Copy because basename may modify the contents of the provided input
    name_copy = strdup(name);
    amxc_string_setf(&dst_path, "%s%s", dst_dir, basename(name_copy));

    fd_in = open(name, O_RDONLY);
    when_true_trace(fd_in < 0, exit, ERROR, "Failed to open %s", name);

    fd_out = creat(amxc_string_get(&dst_path, 0), 0644);
    when_true_trace(fd_out < 0, exit, ERROR, "Failed to create %s", amxc_string_get(&dst_path, 0));

    r_chars = read(fd_in, buf, BUFFERSIZE);
    when_true_trace(r_chars < 0, exit, ERROR, "Failed to read input file");
    while(r_chars > 0) {
        w_chars = write(fd_out, buf, r_chars);
        when_true_trace(w_chars < 0, exit, ERROR, "Failed to write output file");
        r_chars = read(fd_in, buf, BUFFERSIZE);
        when_true_trace(r_chars < 0, exit, ERROR, "Failed to read input file");
    }

    retval = 0;
exit:
    free(name_copy);
    amxc_string_clean(&dst_path);
    if(fd_in > 0) {
        close(fd_in);
    }
    if(fd_out > 0) {
        close(fd_out);
    }
    return retval;
}

static int cp_httpaccess_copy_files(void) {
    int retval = -1;
    amxo_parser_t* parser = cp_get_parser();
    const char* src_dir = NULL;
    const char* dst_dir = NULL;

    when_null(parser, exit);

    src_dir = GET_CHAR(amxo_parser_get_config(parser, "cp_httpaccess_src"), NULL);
    dst_dir = GET_CHAR(amxo_parser_get_config(parser, "cp_httpaccess_dst"), NULL);
    when_false(amxp_dir_is_directory(src_dir), exit);
    SAH_TRACEZ_INFO(ME, "Copy files from %s to %s", src_dir, dst_dir);

    retval = amxp_dir_make(dst_dir, 0755);
    when_failed(retval, exit);
    retval = amxp_dir_scan(src_dir, NULL, false, copy_webpage_file, NULL);
    when_failed(retval, exit);

exit:
    return retval;
}

static bool cp_httpaccess_exists(void) {
    bool retval = true;
    const char* object = "UserInterface.HTTPAccess.[Alias=='captive'].";
    amxc_var_t* result = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxb_get(amxb_be_who_has("UserInterface."), object, 0, &ret, 5);
    result = GETP_ARG(&ret, "0.0");
    if(result == NULL) {
        retval = false;
    }

    amxc_var_clean(&ret);
    return retval;
}

static int cp_httpaccess_set_config(amxb_bus_ctx_t* ctx) {
    int retval = -1;
    amxo_parser_t* parser = cp_get_parser();
    const char* prefix = NULL;
    amxc_var_t ret;
    amxc_var_t values;
    amxc_string_t config_obj;

    amxc_string_init(&config_obj, 0);
    amxc_var_init(&ret);
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    when_null(parser, exit);

    prefix = GET_CHAR(amxo_parser_get_config(parser, "prefix_"), NULL);
    amxc_string_setf(&config_obj, "UserInterface.HTTPAccess.[Alias == 'captive'].%sHTTPConfig.",
                     prefix != NULL ? prefix : "");

    amxc_var_add_key(cstring_t, &values, "Type", "VHost");
    amxc_var_add_key(cstring_t, &values, "VHostDocumentRoot", "/captive/");
    retval = amxb_set(ctx, amxc_string_get(&config_obj, 0), &values, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to set %s, error code: %d",
                         amxc_string_get(&config_obj, 0), retval);
        goto exit;
    }

exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    amxc_string_clean(&config_obj);
    return retval;
}

static int cp_httpaccess_add(amxd_object_t* lan_obj) {
    int retval = -1;
    const char* object = "UserInterface.HTTPAccess.";
    amxb_bus_ctx_t* ctx = amxb_be_who_has(object);
    amxc_string_t* dotted_interface = NULL;
    amxc_var_t ret;
    amxc_var_t values;
    amxc_var_t params;

    amxc_var_init(&ret);
    amxc_var_init(&params);
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    amxd_object_get_params(lan_obj, &params, amxd_dm_access_protected);

    amxc_var_add_key(cstring_t, &values, "AccessType", "LocalAccess");
    amxc_var_add_key(cstring_t, &values, "AllowedPathPrefix", "captive");
    amxc_var_add_key(cstring_t, &values, "Protocol", "HTTP");
    amxc_var_add_key(uint32_t, &values, "Order", 3);
    amxc_var_add_key(uint32_t, &values, "Port", GET_UINT32(&params, "FASPort"));
    dotted_interface = dm_lan_intf_get_dotted(lan_obj);
    amxc_var_add_key(cstring_t, &values, "Interface", amxc_string_get(dotted_interface, 0));

    retval = amxb_add(ctx, object, 0, "captive", &values, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add HTTPAccess instance, error code: %d", retval);
        goto exit;
    }

    retval = cp_httpaccess_set_config(ctx);

exit:
    amxc_string_delete(&dotted_interface);
    amxc_var_clean(&values);
    amxc_var_clean(&params);
    amxc_var_clean(&ret);
    return retval;
}

static int cp_httpaccess_enable(void) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("UserInterface.");
    amxc_var_t ret;
    amxc_var_t values;

    amxc_var_init(&ret);
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(bool, &values, "Enable", true);
    retval = amxb_set(ctx, "UserInterface.HTTPAccess.[Alias == 'captive'].", &values, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to enable HTTPAccess instance");
        goto exit;
    }

exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    return retval;
}

int cp_httpaccess_init(amxd_object_t* lan_obj) {
    int retval = 0;

    SAH_TRACEZ_INFO(ME, "Initialize HTTPAccess");
    retval = cp_httpaccess_copy_files();
    if(retval != 0) {
        SAH_TRACEZ_NOTICE(ME, "Did not copy webpage files");
    }

    if(!cp_httpaccess_exists()) {
        retval = cp_httpaccess_add(lan_obj);
        when_failed(retval, exit);
    } else {
        SAH_TRACEZ_INFO(ME, "HTTPAccess instance already exists");
    }
    retval = cp_httpaccess_enable();
    when_failed(retval, exit);

exit:
    return retval;
}
