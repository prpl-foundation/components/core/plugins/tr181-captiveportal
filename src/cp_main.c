/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>

#include <debug/sahtrace.h>

#include "captive_portal.h"
#include "cp_wan.h"

static cp_app_t app;

static int opennds_ctrl_build_cmd(amxc_array_t* cmd, UNUSED amxc_var_t* settings) {
    amxc_array_append_data(cmd, strdup("opennds"));
    amxc_array_append_data(cmd, strdup("-f"));
    return 0;
}

static int cp_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Starting %s", ME);
    app.dm = dm;
    app.parser = parser;

    amxp_sigmngr_add_signal(NULL, PROC_STOPPED);
    retval = amxp_proc_ctrl_new(&app.opennds_ctrl, opennds_ctrl_build_cmd);
    when_failed(retval, exit);

    netmodel_initialize();

exit:
    return retval;
}

static void cp_clean(void) {
    amxp_signal_t* sig = amxp_sigmngr_find_signal(NULL, PROC_STOPPED);

    amxp_sigmngr_remove_signal(NULL, PROC_STOPPED);
    amxp_signal_delete(&sig);

    if(app.wan_query != NULL) {
        cp_wan_query_close();
    }

    netmodel_cleanup();
    // amxp_proc_ctrl_delete can send SIGKILL too quickly
    if(amxp_subproc_is_running(app.opennds_ctrl->proc)) {
        amxp_subproc_kill(app.opennds_ctrl->proc, SIGTERM);
        amxp_subproc_wait(app.opennds_ctrl->proc, 10000);
    }
    amxp_proc_ctrl_delete(&app.opennds_ctrl);
    app.opennds_ctrl = NULL;
    app.parser = NULL;
    app.dm = NULL;
}

amxd_dm_t* cp_get_dm(void) {
    return app.dm;
}

amxo_parser_t* cp_get_parser(void) {
    return app.parser;
}

amxp_proc_ctrl_t* cp_get_proc_ctrl(void) {
    return app.opennds_ctrl;
}

cp_app_t* cp_get_app(void) {
    return &app;
}

int _cp_main(int reason,
             amxd_dm_t* dm,
             amxo_parser_t* parser) {
    int retval = 0;
    switch(reason) {
    case AMXO_START:
        retval = cp_init(dm, parser);
        break;
    case AMXO_STOP:
        cp_clean();
        break;
    }
    return retval;
}
