/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <unistd.h>
#include <string.h>

#include "captive_portal.h"
#include "cp_opennds.h"
#include "cp_utils.h"
#include "cp_wan.h"
#include "dm_cp.h"
#include "dm_lan_intf.h"

static void process_stopped_cb(UNUSED const char* const sig_name,
                               UNUSED const amxc_var_t* const data,
                               UNUSED void* const priv) {
    amxd_object_t* root_obj = amxd_dm_findf(cp_get_dm(), "CaptivePortal.");
    amxd_object_t* lan_obj = amxd_object_findf(root_obj, "LANInterface.1.");    // For now we just use a single interface
    char* state = amxd_object_get_value(cstring_t, root_obj, "State", NULL);

    SAH_TRACEZ_INFO(ME, "Subprocess stopped");
    when_str_empty(state, exit);

    unlink("/tmp/opennds.lock");
    amxp_slot_disconnect(NULL, PROC_STOPPED, process_stopped_cb);

    if(strcmp(state, "Stopping") == 0) {
        dm_cp_set_value(NULL, "State", "Stopped");
        dm_cp_set_value(lan_obj, "Status", "Enabled");
    } else if(strcmp(state, "Restart") == 0) {
        cp_wan_network_down(state, lan_obj);
    }

exit:
    free(state);
    return;
}

static amxc_llist_t* get_urls_list(const char* portal_urls) {
    amxc_string_t urls_str;
    amxc_llist_t* urls_list = NULL;

    amxc_string_init(&urls_str, 0);
    amxc_llist_new(&urls_list);

    when_str_empty(portal_urls, exit);

    amxc_string_setf(&urls_str, "%s", portal_urls);
    amxc_string_split_to_llist(&urls_str, urls_list, ',');

exit:
    if(portal_urls == NULL) {
        amxc_llist_delete(&urls_list, NULL);
        urls_list = NULL;
    }
    amxc_string_clean(&urls_str);
    return urls_list;
}

static void args_set_alias_and_name(amxc_var_t* const args,
                                    const amxc_string_t* const url,
                                    const char* netdev_name,
                                    const int i,
                                    amxc_var_t* const dns_aliases) {
    amxc_string_t alias;
    amxc_var_t* alias_param = NULL;
    amxc_var_t* name_param = NULL;

    amxc_string_init(&alias, 0);
    amxc_string_setf(&alias, "captive-portal-%s-%d", netdev_name, i);

    alias_param = GET_ARG(args, "Alias");
    if(alias_param == NULL) {
        alias_param = amxc_var_add_key(cstring_t, args, "Alias", "");
    }
    amxc_var_set(cstring_t, alias_param, amxc_string_get(&alias, 0));
    // Save Alias of created instance for later to clean up
    amxc_var_add(cstring_t, dns_aliases, amxc_string_get(&alias, 0));

    name_param = GET_ARG(args, "Name");
    if(name_param == NULL) {
        name_param = amxc_var_add_key(cstring_t, args, "Name", "");
    }
    amxc_var_set(cstring_t, name_param, amxc_string_get(url, 0));

    amxc_string_clean(&alias);
}

int cp_opennds_dns_redirect_start(amxd_object_t* const lan_obj) {
    amxd_object_t* root_obj = amxd_dm_findf(cp_get_dm(), "CaptivePortal");
    char* portal_urls = amxd_object_get_value(cstring_t, root_obj, "PortalURLs", NULL);
    amxb_bus_ctx_t* ctx = amxb_be_who_has("DNS.");
    amxc_string_t* interface = dm_lan_intf_get_dotted(lan_obj);
    amxc_var_t* ip_addr = netmodel_luckyAddr(amxc_string_get(interface, 0), "ipv4", "down");
    const char* ip_address = GET_CHAR(ip_addr, "Address");
    const char* netdev_name = GET_CHAR(ip_addr, "NetDevName");
    amxc_llist_t* urls_list = NULL;
    cp_lan_intf_t* lan_intf = NULL;
    int i = 0;
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* dns_aliases = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    when_null_trace(lan_obj, exit, WARNING, "LAN object is NULL");
    amxc_var_new(&dns_aliases);
    amxc_var_set_type(dns_aliases, AMXC_VAR_ID_LIST);
    lan_intf = (cp_lan_intf_t*) lan_obj->priv;
    lan_intf->dns_aliases = dns_aliases;

    urls_list = get_urls_list(portal_urls);
    when_null_trace(urls_list, exit, WARNING, "No Portal URLs to redirect");
    when_str_empty(ip_address, exit);

    amxc_llist_for_each(it, urls_list) {
        amxc_string_t* url = amxc_string_from_llist_it(it);
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "Origin", "DHCPv4");
        amxc_var_add_key(bool, &args, "Enable", true);
        amxc_var_add_key(cstring_t, &args, "Interface", amxc_string_get(interface, 0));
        amxc_var_add_key(cstring_t, &args, "IPAddresses", ip_address);
        args_set_alias_and_name(&args, url, netdev_name, i, dns_aliases);
        retval = amxb_call(ctx, "DNS.", "SetHost", &args, &ret, 5);
        if(retval != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to call DNS.SetHost()");
            goto exit;
        }
        i++;
    }

exit:
    amxc_llist_delete(&urls_list, amxc_string_list_it_free);
    amxc_var_delete(&ip_addr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_delete(&interface);
    free(portal_urls);
    return retval;
}

int cp_opennds_dns_redirect_stop(amxd_object_t* const lan_obj) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("DNS.");
    cp_lan_intf_t* lan_intf = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null(lan_obj, exit);

    lan_intf = (cp_lan_intf_t*) lan_obj->priv;

    amxc_var_for_each(entry, lan_intf->dns_aliases) {
        const char* alias = amxc_var_constcast(cstring_t, entry);
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "Alias", alias);
        retval = amxb_call(ctx, "DNS.", "RemoveHost", &args, &ret, 5);
        if(retval != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to call DNS.RemoveHost(Alias = %s)", alias);
        }
    }

exit:
    if(lan_obj != NULL) {
        amxc_var_delete(&lan_intf->dns_aliases);
        lan_intf->dns_aliases = NULL;
    }
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return retval;
}

int cp_opennds_start(amxd_object_t* const lan_obj) {
    int retval = -1;
    amxp_proc_ctrl_t* opennds_ctrl = cp_get_proc_ctrl();

    SAH_TRACEZ_INFO(ME, "Starting opennds process");
    retval = amxp_proc_ctrl_start(opennds_ctrl, 0, NULL);
    when_failed(retval, exit);

    amxp_slot_connect(NULL, PROC_STOPPED, NULL, process_stopped_cb, NULL);
    dm_cp_set_value(NULL, "State", "Running");
    dm_cp_set_value(lan_obj, "Status", "Intercepting");

exit:
    return retval;
}

void cp_opennds_stop(void) {
    amxp_proc_ctrl_t* opennds_ctrl = cp_get_proc_ctrl();

    // don't use amxp_proc_ctrl_stop, because it kills (SIGKILL) opennds before firewall is cleaned
    SAH_TRACEZ_INFO(ME, "Stopping opennds process");
    amxp_subproc_kill(opennds_ctrl->proc, SIGTERM);
    dm_cp_set_value(NULL, "State", "Stopping");
}
