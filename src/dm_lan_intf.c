/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>

#include "captive_portal.h"
#include "dm_cp.h"
#include "dm_lan_intf.h"
#include "cp_wan.h"
#include "cp_utils.h"
#include "cp_config.h"
#include "cp_opennds.h"
#include "cp_httpaccess.h"

static int cp_lan_intf_new(cp_lan_intf_t** lan_intf) {
    int retval = -1;

    when_null(lan_intf, exit);
    when_not_null(*lan_intf, exit);

    *lan_intf = calloc(1, sizeof(cp_lan_intf_t));
    when_null(*lan_intf, exit);

    retval = 0;
exit:
    return retval;
}

static void cp_lan_intf_delete(cp_lan_intf_t** lan_intf) {
    when_null(lan_intf, exit);
    when_null(*lan_intf, exit);

    if((*lan_intf)->dns_aliases != NULL) {
        amxc_var_delete(&(*lan_intf)->dns_aliases);
        (*lan_intf)->dns_aliases = NULL;
    }
    if((*lan_intf)->lan_ip_query != NULL) {
        netmodel_closeQuery((*lan_intf)->lan_ip_query);
        (*lan_intf)->lan_ip_query = NULL;
    }
    free(*lan_intf);
    *lan_intf = NULL;

exit:
    return;
}

static amxc_string_t* get_encoded_dhcpv4_option(amxd_object_t* const lan_obj,
                                                const amxc_string_t* const interface) {
    char* ip_address = netmodel_luckyAddrAddress(amxc_string_get(interface, 0), "ipv4", "down");
    char* path = amxd_object_get_value(cstring_t, lan_obj, "FASPath", NULL);
    uint32_t port = amxd_object_get_value(uint32_t, lan_obj, "FASPort", NULL);
    amxc_string_t* encoded_value = NULL;
    amxc_string_t value;

    amxc_string_init(&value, 0);
    amxc_string_new(&encoded_value, 0);

    when_str_empty_trace(ip_address, exit, ERROR, "Failed to get LAN IP address");
    when_str_empty(path, exit);

    amxc_string_setf(&value, "http://%s:%d%s", ip_address, port, path);
    SAH_TRACEZ_INFO(ME, "Encoding option 114 with value %s", amxc_string_get(&value, 0));
    amxc_string_bytes_2_hex_binary(encoded_value, amxc_string_get(&value, 0), amxc_string_text_length(&value), NULL);
    SAH_TRACEZ_INFO(ME, "Encoded value = %s", amxc_string_get(encoded_value, 0));

exit:
    if(amxc_string_text_length(encoded_value) == 0) {
        amxc_string_delete(&encoded_value);
        encoded_value = NULL;
    }
    amxc_string_clean(&value);
    free(ip_address);
    free(path);
    return encoded_value;
}

static int lan_dhcpv4_option_add(amxb_bus_ctx_t* const ctx,
                                 const amxc_string_t* const option,
                                 amxd_object_t* const lan_obj,
                                 const amxc_string_t* const dotted_intf) {
    amxc_string_t* encoded_value = get_encoded_dhcpv4_option(lan_obj, dotted_intf);
    int retval = -1;
    amxc_var_t values;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    when_null_trace(encoded_value, exit, ERROR, "Encoded DHCP option is NULL");

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &values, "Enable", true);
    amxc_var_add_key(bool, &values, "X_PRPL-COM_Force", true);
    amxc_var_add_key(uint8_t, &values, "Tag", 114);
    amxc_var_add_key(cstring_t, &values, "Value", amxc_string_get(encoded_value, 0));

    retval = amxb_add(ctx, amxc_string_get(option, 0), 0, "captive-portal", &values, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add %s instance", amxc_string_get(option, 0));
        goto exit;
    }

exit:
    amxc_string_delete(&encoded_value);
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    return retval;
}

static int lan_dhcpv4_option_set(amxb_bus_ctx_t* const ctx,
                                 const char* path,
                                 amxd_object_t* const lan_obj,
                                 const amxc_string_t* const dotted_intf,
                                 const bool enable) {
    amxc_string_t* encoded_value = get_encoded_dhcpv4_option(lan_obj, dotted_intf);
    int retval = -1;
    amxc_var_t values;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    when_null_trace(encoded_value, exit, ERROR, "Encoded DHCP option is NULL");

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Value", amxc_string_get(encoded_value, 0));
    amxc_var_add_key(bool, &values, "Enable", enable);

    retval = amxb_set(ctx, path, &values, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to set %s", path);
        goto exit;
    }

exit:
    amxc_string_delete(&encoded_value);
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    return retval;
}

static char* get_dhcpv4_pool(amxb_bus_ctx_t* const ctx, const amxc_string_t* const dotted_intf) {
    const char* alias = NULL;
    const char* pool = NULL;
    char* result = NULL;
    amxc_var_t ret;
    amxc_string_t path;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    amxb_get(ctx, amxc_string_get(dotted_intf, 0), 0, &ret, 5);
    alias = GETP_CHAR(&ret, "0.0.Alias");
    when_str_empty(alias, exit);

    amxc_string_setf(&path, "Device.DHCPv4.Server.Pool.[Alias=='%s'].", alias);
    amxb_get(ctx, amxc_string_get(&path, 0), 0, &ret, 5);
    pool = amxc_var_key(GETP_ARG(&ret, "0.0"));
    when_str_empty(pool, exit);

    result = strdup(pool);

exit:
    amxc_string_clean(&path);
    amxc_var_clean(&ret);
    return result;
}

static int lan_dhcpv4_option_update(amxd_object_t* const object, const bool enable) {
    int retval = -1;
    char* pool = NULL;
    amxc_string_t* dotted_intf = dm_lan_intf_get_dotted(object);
    amxb_bus_ctx_t* ctx = amxb_be_who_has("Device.");
    amxc_var_t* result = NULL;
    amxc_string_t option;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_string_init(&option, 0);

    when_null(dotted_intf, exit);
    pool = get_dhcpv4_pool(ctx, dotted_intf);
    when_str_empty(pool, exit);

    amxc_string_setf(&option, "%sOption.[Tag==114].", pool);

    SAH_TRACEZ_INFO(ME, "Try to get %s", amxc_string_get(&option, 0));
    amxb_get(ctx, amxc_string_get(&option, 0), 0, &ret, 5);
    result = GETP_ARG(&ret, "0.0");
    // If option does not exist, add it, otherwise update the Value
    if((result == NULL) && enable) {
        amxc_string_shrink(&option, 11);
        SAH_TRACEZ_INFO(ME, "Adding DHCP Option 114");
        retval = lan_dhcpv4_option_add(ctx, &option, object, dotted_intf);
    } else {
        SAH_TRACEZ_INFO(ME, "Setting DHCP Option 114");
        retval = lan_dhcpv4_option_set(ctx, amxc_var_key(result), object, dotted_intf, enable);
    }

exit:
    free(pool);
    amxc_var_clean(&ret);
    amxc_string_clean(&option);
    amxc_string_delete(&dotted_intf);
    return retval;
}

static void lan_ip_retrieved(UNUSED const char* sig_name,
                             const amxc_var_t* data,
                             UNUSED void* priv) {
    const char* ip = amxc_var_constcast(cstring_t, data);
    amxd_object_t* object = (amxd_object_t*) priv;
    amxc_string_t* dotted_path = NULL;
    bool wan_up = false;
    char* state = NULL;

    when_str_empty_trace(ip, exit, WARNING, "LAN IP is empty string");

    SAH_TRACEZ_INFO(ME, "Got LAN IP %s", ip);
    cp_config_generate_file(object);
    lan_dhcpv4_option_update(object, true);
    dm_cp_set_value(object, "Status", "Enabled");

    dotted_path = dm_cp_wan_intf_get_dotted();
    when_null(dotted_path, exit);

    state = dm_cp_get_state();
    if(strcmp(state, "Running") == 0) {
        cp_opennds_dns_redirect_stop(object);
        cp_opennds_dns_redirect_start(object);
    }

    wan_up = netmodel_isUp(amxc_string_get(dotted_path, 0), "ipv4-up", "down");
    if(wan_up) {
        SAH_TRACEZ_INFO(ME, "WAN is up");
        cp_wan_network_up(state, object);
    } else {
        SAH_TRACEZ_INFO(ME, "WAN is down");
        cp_wan_network_down(state, object);
    }

exit:
    amxc_string_delete(&dotted_path);
    free(state);
    return;
}

static void lan_query_open(amxd_object_t* object) {
    amxc_string_t* dotted_path = dm_lan_intf_get_dotted(object);
    cp_lan_intf_t* lan_intf = (cp_lan_intf_t*) object->priv;

    when_null_trace(dotted_path, exit, ERROR, "No interface configured");
    when_not_null(lan_intf->lan_ip_query, exit);

    SAH_TRACEZ_INFO(ME, "Opening luckyAddrAddress query for %s", amxc_string_get(dotted_path, 0));
    lan_intf->lan_ip_query = netmodel_openQuery_luckyAddrAddress(amxc_string_get(dotted_path, 0), ME, "ipv4",
                                                                 "down", lan_ip_retrieved, object);
    when_null_trace(lan_intf->lan_ip_query, exit, ERROR, "Failed to open query");

exit:
    amxc_string_delete(&dotted_path);
}

static void lan_query_close(amxd_object_t* const object) {
    cp_lan_intf_t* lan_intf = (cp_lan_intf_t*) object->priv;
    netmodel_closeQuery(lan_intf->lan_ip_query);
    lan_intf->lan_ip_query = NULL;
}

void dm_lan_added_and_enabled(amxd_object_t* const object) {
    if(object->priv == NULL) {
        cp_lan_intf_t* lan_intf = NULL;
        int retval = cp_lan_intf_new(&lan_intf);
        when_failed(retval, exit);
        object->priv = lan_intf;
    }
    if(!dm_cp_get_enable()) {
        SAH_TRACEZ_INFO(ME, "Captive portal globally disabled");
        goto exit;
    }
    lan_query_open(object);
    cp_httpaccess_init(object);

exit:
    return;
}

void _dm_lan_intf_added(UNUSED const char* const sig_name,
                        const amxc_var_t* const data,
                        UNUSED void* const priv) {
    amxd_object_t* object = amxd_dm_signal_get_object(cp_get_dm(), data);
    object = amxd_object_get_instance(object, NULL, GET_UINT32(data, "index"));

    when_null(object, exit);

    SAH_TRACEZ_INFO(ME, "Enabled LANInterface instance added");
    dm_lan_added_and_enabled(object);

exit:
    return;
}

void _dm_lan_intf_enabled(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxd_object_t* object = amxd_dm_signal_get_object(cp_get_dm(), data);

    when_null(object, exit);

    SAH_TRACEZ_INFO(ME, "LANInterface instance enabled");
    dm_lan_added_and_enabled(object);

exit:
    return;
}

void _dm_lan_intf_disabled(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    amxd_object_t* object = amxd_dm_signal_get_object(cp_get_dm(), data);
    amxp_proc_ctrl_t* opennds_ctrl = cp_get_proc_ctrl();

    SAH_TRACEZ_INFO(ME, "LANInterface instance disabled");
    when_null(object, exit);

    lan_query_close(object);
    lan_dhcpv4_option_update(object, false);

    when_null(opennds_ctrl, exit);
    if(amxp_subproc_is_running(opennds_ctrl->proc)) {
        cp_opennds_stop();
        cp_opennds_dns_redirect_stop(object);
    }

    dm_cp_set_value(object, "Status", "Disabled");

exit:
    return;
}

amxd_status_t _dm_lan_intf_cleanup(amxd_object_t* object,
                                   UNUSED amxd_param_t* param,
                                   amxd_action_t reason,
                                   UNUSED const amxc_var_t* const args,
                                   UNUSED amxc_var_t* const retval,
                                   UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;
    cp_lan_intf_t* lan_intf = NULL;

    when_null(object, exit);
    when_null(object->priv, exit);
    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    lan_intf = (cp_lan_intf_t*) object->priv;
    cp_lan_intf_delete(&lan_intf);
    object->priv = NULL;

exit:
    return status;
}

amxc_string_t* dm_lan_intf_get_dotted(amxd_object_t* const lan_obj) {
    char* interface = amxd_object_get_value(cstring_t, lan_obj, "Interface", NULL);
    amxc_string_t* dotted_intf = NULL;

    when_str_empty(interface, exit);
    dotted_intf = cp_utils_add_dot(interface);

exit:
    free(interface);
    return dotted_intf;
}
