# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.5.4 - 2024-07-28(06:32:36 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v0.5.3 - 2024-07-18(14:33:53 +0000)

### Other

- IP.wan and IP.lan not always available when loading defaults odl

## Release v0.5.2 - 2024-04-10(10:04:30 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.5.1 - 2024-03-27(16:36:54 +0000)

### Fixes

- some instances should not be reboot/upgrade persistent

## Release v0.5.0 - 2024-02-05(16:22:54 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.4.0 - 2024-01-17(09:31:26 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.3.2 - 2023-12-07(09:56:20 +0000)

### Other

- [Captive Portal] Changes to baf also need to be reflected in odl settings

## Release v0.3.1 - 2023-12-06(09:33:52 +0000)

### Other

- Prefix odl files in defaults directory with a number

## Release v0.3.0 - 2023-12-05(12:26:44 +0000)

### New

- [Captive Portal] Need to handle changes in LAN IP address

## Release v0.2.0 - 2023-12-01(15:52:57 +0000)

### New

- [Captive Portal] Host captive portal page via HTTPAccess
- Remove file lock when opennds stops

## Release v0.1.5 - 2023-11-28(11:18:03 +0000)

### Other

- Opensource component to prpl-foundation gitlab

## Release v0.1.4 - 2023-11-28(08:11:41 +0000)

### Fixes

- [Captive Portal] Redirecting to local WebUI does not work

## Release v0.1.3 - 2023-11-23(15:33:20 +0000)

### Fixes

- [DNS] Hosts must not be persistent

## Release v0.1.2 - 2023-11-14(08:14:42 +0000)

### Other

- [Captive Portal] Disable captive portal until webpage is available

## Release v0.1.1 - 2023-11-13(10:01:40 +0000)

### Other

- Add runtime dependency on opennds

## Release v0.1.0 - 2023-11-02(15:03:16 +0000)

### New

- [Captive Portal] Implement amx captive portal plugin

