MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)

SOURCES = $(wildcard $(SRCDIR)/*.c)

MOCK_WRAP = netmodel_openQuery_isUp \
            netmodel_openQuery_luckyAddrAddress \
            netmodel_closeQuery \
            netmodel_isUp \
            netmodel_luckyAddr \
            netmodel_luckyAddrAddress \
            amxp_proc_ctrl_start \
            amxp_subproc_kill \
            amxp_subproc_wait

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
          $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../mocks -I../dummy_back_end -I../common \
          -fkeep-inline-functions -fkeep-static-functions \
          -Wno-format-nonliteral -Wno-unused-variable -Wno-unused-but-set-variable \
          $(shell pkg-config --cflags cmocka) -pthread

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
           $(shell pkg-config --libs cmocka) -lamxc -lamxp -lamxd -lamxb -lamxo \
           -lsahtrace -lnetmodel

LDFLAGS += $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
