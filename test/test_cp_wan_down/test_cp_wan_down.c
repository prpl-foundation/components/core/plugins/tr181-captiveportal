/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include "captive_portal.h"
#include "dm_cp.h"
#include "dm_lan_intf.h"

#include "mock.h"
#include "common.h"
#include "dummy_be.h"
#include "test_cp_wan_down.h"

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;
static const char* config_odl = "../common/config.odl";
static const char* cp_definition = "../../odl/tr181-captiveportal_definition.odl";
static const char* cp_defaults = "../common/tr181-captiveportal_defaults.odl";
static const char* dns_odl = "../common/dns.odl";
static const char* device_odl = "../common/device.odl";
static const char* userintf_odl = "../common/tr181-userinterface_definition.odl";

static int count_commas(const char* str) {
    int count = 0;
    for(int i = 0; str[i] != '\0'; i++) {
        if(str[i] == ',') {
            count++;
        }
    }
    return count;
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    assert_int_equal(test_register_dummy_be(), 0);
    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "dm_cp_enabled", AMXO_FUNC(_dm_cp_enabled));
    amxo_resolver_ftab_add(&parser, "dm_cp_disabled", AMXO_FUNC(_dm_cp_disabled));
    amxo_resolver_ftab_add(&parser, "dm_lan_intf_cleanup", AMXO_FUNC(_dm_lan_intf_cleanup));
    amxo_resolver_ftab_add(&parser, "dm_lan_intf_added", AMXO_FUNC(_dm_lan_intf_added));
    amxo_resolver_ftab_add(&parser, "dm_lan_intf_enabled", AMXO_FUNC(_dm_lan_intf_enabled));
    amxo_resolver_ftab_add(&parser, "dm_lan_intf_disabled", AMXO_FUNC(_dm_lan_intf_disabled));
    amxo_resolver_ftab_add(&parser, "SetHost", AMXO_FUNC(_DNS_SetHost));
    amxo_resolver_ftab_add(&parser, "RemoveHost", AMXO_FUNC(_DNS_RemoveHost));

    assert_int_equal(amxo_parser_parse_file(&parser, config_odl, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, cp_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, dns_odl, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, device_odl, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, userintf_odl, root_obj), 0);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxp_sigmngr_add_signal(NULL, PROC_STOPPED), 0);

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_cp_main(1, &dm, &parser), 0);
    handle_events();

    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    system("rm -r webpage/");
    return 0;
}

void test_can_start_plugin(UNUSED void** state) {
    assert_int_equal(_cp_main(0, &dm, &parser), 0);
    handle_events();
}

void test_can_load_defaults(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxc_var_t params;

    amxc_var_init(&params);

    will_return(__wrap_netmodel_openQuery_isUp, 0); // WAN query

    object = amxd_dm_get_root(&dm);
    assert_int_equal(amxo_parser_parse_file(&parser, cp_defaults, object), 0);
    handle_events();

    // Check that HTTPAccess instance is created and files have been moved
    object = amxd_dm_findf(&dm, "UserInterface.HTTPAccess.[Alias == 'captive'].");
    assert_non_null(object);
    amxd_object_get_params(object, &params, amxd_dm_access_private);
    amxc_var_dump(&params, STDOUT_FILENO);

    assert_true(amxp_dir_is_directory("webpage/"));
    assert_false(amxp_dir_is_empty("webpage/"));

    amxc_var_clean(&params);
}

// Test to make sure subprocess does not start if LAN initialization is not done yet
void test_lan_init_not_done(UNUSED void** state) {
    amxc_var_t data;
    amxd_object_t* object = NULL;
    netmodel_callback_t intf_up = NULL;
    char* subproc_state = NULL;

    amxc_var_init(&data);

    // LAN is temporarily down on startup, but nothing should happen until LAN init is done
    amxc_var_set(bool, &data, false);

    intf_up = test_netmodel_wan_intf_up_callback_get();
    assert_non_null(intf_up);

    intf_up(NULL, &data, NULL);
    handle_events();

    object = amxd_dm_findf(&dm, "CaptivePortal.");
    subproc_state = amxd_object_get_value(cstring_t, object, "State", NULL);
    assert_string_equal(subproc_state, "Init");

    free(subproc_state);
}

// No LAN IP on startup does nothing
void test_lan_intf_down_cb(UNUSED void** state) {
    amxc_var_t data;
    netmodel_callback_t lan_ip_cb = NULL;
    amxd_object_t* object = amxd_dm_findf(&dm, "CaptivePortal.LANInterface.1.");

    amxc_var_init(&data);
    amxc_var_set(cstring_t, &data, "");

    lan_ip_cb = test_netmodel_lan_intf_ip_callback_get();
    assert_non_null(lan_ip_cb);

    lan_ip_cb(NULL, &data, object);
    handle_events();
    amxc_var_clean(&data);
}

// LAN interface up when WAN is up will only generate config file and set DHCP option
void test_lan_intf_up_when_wan_up(UNUSED void** state) {
    char* value = NULL;
    amxd_object_t* object = amxd_dm_findf(&dm, "CaptivePortal.LANInterface.1.");
    char* subproc_state = NULL;
    netmodel_callback_t lan_ip_cb = NULL;
    amxc_var_t data;

    amxc_var_init(&data);

    amxc_var_set(cstring_t, &data, "192.168.1.1");

    lan_ip_cb = test_netmodel_lan_intf_ip_callback_get();
    assert_non_null(lan_ip_cb);

    // Assume WAN up
    will_return(__wrap_netmodel_isUp, true);
    lan_ip_cb(NULL, &data, object);

    handle_events();
    object = amxd_dm_findf(&dm, "Device.DHCPv4.Server.Pool.lan.Option.captive-portal.");
    assert_non_null(object);
    value = amxd_object_get_value(cstring_t, object, "Value", NULL);
    printf("Value = %s\n", value);
    // Value starts with 'http://' hexadecimally encoded
    assert_true(strncmp(value, "687474703A2F2F", 14) == 0);

    amxc_var_clean(&data);
    free(value);
}

void test_lan_intf_up_when_wan_down(UNUSED void** state) {
    char* value = NULL;
    amxc_var_t data;
    amxc_var_t params;
    const char* portal_urls = NULL;
    amxd_object_t* object = amxd_dm_findf(&dm, "CaptivePortal.LANInterface.1.");
    char* subproc_state = NULL;
    netmodel_callback_t lan_ip_cb = NULL;

    amxc_var_init(&data);
    amxc_var_init(&params);

    amxc_var_set(cstring_t, &data, "192.168.1.1");

    lan_ip_cb = test_netmodel_lan_intf_ip_callback_get();
    assert_non_null(lan_ip_cb);

    // Assume WAN down
    will_return(__wrap_netmodel_isUp, false);
    will_return(__wrap_amxp_proc_ctrl_start, 0);
    lan_ip_cb(NULL, &data, object);
    handle_events();

    object = amxd_dm_findf(&dm, "CaptivePortal");
    amxd_object_get_params(object, &params, amxd_dm_access_private);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "State"), "Running");
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    // Check number of calls to DNS.SetHost
    portal_urls = GET_CHAR(&params, "PortalURLs");
    assert_string_not_equal(portal_urls, "");
    assert_int_equal(count_commas(portal_urls) + 1, common_get_num_cbs());

    object = amxd_dm_findf(&dm, "CaptivePortal.LANInterface.1.");
    amxd_object_get_params(object, &params, amxd_dm_access_private);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Intercepting");

    amxc_var_clean(&params);
    amxc_var_clean(&data);
}

void test_wan_up_stops_subproc(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t params;
    amxd_object_t* object = NULL;
    netmodel_callback_t intf_up = NULL;

    amxc_var_init(&params);
    amxc_var_init(&data);
    amxc_var_set(bool, &data, true);

    intf_up = test_netmodel_wan_intf_up_callback_get();
    assert_non_null(intf_up);

    will_return(__wrap_amxp_subproc_kill, 0);
    intf_up(NULL, &data, NULL);
    handle_events();

    // Number of calls to SetHost is equal to number of calls to RemoveHost
    assert_int_equal(common_get_num_cbs(), 0);

    object = amxd_dm_findf(&dm, "CaptivePortal");
    amxd_object_get_params(object, &params, amxd_dm_access_private);
    amxc_var_dump(&params, STDOUT_FILENO);
    // Status is Enabled until proc:stopped signal is received
    assert_string_equal(GET_CHAR(&params, "State"), "Stopping");
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    object = amxd_dm_findf(&dm, "CaptivePortal.LANInterface.1.");
    amxd_object_get_params(object, &params, amxd_dm_access_private);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Intercepting");

    amxc_var_clean(&data);
    amxc_var_clean(&params);
}

void test_state_updates_on_proc_stopped(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxc_var_t params;

    amxc_var_init(&params);

    assert_int_equal(amxp_sigmngr_emit_signal(NULL, PROC_STOPPED, NULL), 0);
    handle_events();

    object = amxd_dm_findf(&dm, "CaptivePortal");
    amxd_object_get_params(object, &params, amxd_dm_access_private);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "State"), "Stopped");
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled"); // Enabled, but idle

    object = amxd_dm_findf(&dm, "CaptivePortal.LANInterface.1.");
    amxd_object_get_params(object, &params, amxd_dm_access_private);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled"); // Enabled, but idle

    amxc_var_clean(&params);
}

void test_toggle_lan_intf_when_wan_down(UNUSED void** state) {
    amxc_var_t data;
    amxd_trans_t trans;
    netmodel_callback_t intf_up = NULL;
    netmodel_callback_t lan_ip_cb = NULL;
    amxd_object_t* object = amxd_dm_findf(&dm, "CaptivePortal.LANInterface.1.");

    amxc_var_init(&data);
    amxc_var_set(bool, &data, false);

    intf_up = test_netmodel_wan_intf_up_callback_get();
    assert_non_null(intf_up);

    will_return(__wrap_amxp_proc_ctrl_start, 0);
    intf_up(NULL, &data, NULL);
    handle_events();

    // Disable
    will_return(__wrap_amxp_subproc_kill, 0);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "CaptivePortal.LANInterface.1.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    // Handle stopped signal
    assert_int_equal(amxp_sigmngr_emit_signal(NULL, PROC_STOPPED, NULL), 0);
    handle_events();

    // Enable LANInterface
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "CaptivePortal.LANInterface.1.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    // Call open query callback function
    lan_ip_cb = test_netmodel_lan_intf_ip_callback_get();
    assert_non_null(lan_ip_cb);

    will_return(__wrap_netmodel_isUp, false);
    will_return(__wrap_amxp_proc_ctrl_start, 0);
    amxc_var_set(cstring_t, &data, "192.168.1.1");
    lan_ip_cb(NULL, &data, object);
    handle_events();

    // Set WAN back up
    will_return(__wrap_amxp_subproc_kill, 0);
    amxc_var_set(bool, &data, true);
    intf_up(NULL, &data, NULL);
    handle_events();
    assert_int_equal(amxp_sigmngr_emit_signal(NULL, PROC_STOPPED, NULL), 0);
    handle_events();
}

// On IP change, update DHCP option and DNS Hosts if needed
void test_can_handle_lan_ip_change(UNUSED void** state) {
    netmodel_callback_t ip_changed = test_netmodel_lan_intf_ip_callback_get();
    netmodel_callback_t wan_intf_up = test_netmodel_wan_intf_up_callback_get();
    amxd_object_t* object = NULL;
    const char* new_ip = "10.0.0.1";
    char* fsm_state = NULL;
    char* value_before = NULL;
    char* value_after = NULL;
    amxc_var_t data;
    amxc_var_t values;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&values);
    amxc_var_init(&ret);

    // Fetch DHCPv4 Option value before IP is changed
    object = amxd_dm_findf(&dm, "Device.DHCPv4.Server.Pool.lan.Option.captive-portal.");
    assert_non_null(object);
    value_before = amxd_object_get_value(cstring_t, object, "Value", NULL);
    printf("Value = %s\n", value_before);

    // Set WAN Down
    amxc_var_set(bool, &data, false);
    will_return(__wrap_amxp_proc_ctrl_start, 0);
    wan_intf_up(NULL, &data, NULL);
    handle_events();

    // Get state to make sure process is running
    fsm_state = dm_cp_get_state();
    assert_string_equal(fsm_state, "Running");

    // Update IP address in dm
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "IPAddress", new_ip);
    assert_int_equal(amxb_set(bus_ctx, "Device.IP.Interface.lan.IPv4Address.lan.", &values, &ret, 5), 0);

    // Call IP changed callback function
    amxc_var_set(cstring_t, &data, new_ip);
    object = amxd_dm_findf(&dm, "CaptivePortal.LANInterface.1.");
    will_return(__wrap_netmodel_isUp, false);
    ip_changed(NULL, &data, object);

    // Fetch DHCPv4 Option value after IP is changed
    object = amxd_dm_findf(&dm, "Device.DHCPv4.Server.Pool.lan.Option.captive-portal.");
    assert_non_null(object);
    value_after = amxd_object_get_value(cstring_t, object, "Value", NULL);
    printf("Value = %s\n", value_after);
    assert_string_not_equal(value_before, value_after);

    // Check DNS Hosts
    assert_int_equal(amxb_get(bus_ctx, "DNS.Host.", INT32_MAX, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_string_equal(GETP_CHAR(&ret, "0.0.IPAddress"), new_ip);

    // Set WAN back up
    will_return(__wrap_amxp_subproc_kill, 0);
    amxc_var_set(bool, &data, true);
    wan_intf_up(NULL, &data, NULL);
    handle_events();
    assert_int_equal(amxp_sigmngr_emit_signal(NULL, PROC_STOPPED, NULL), 0);
    handle_events();

    free(fsm_state);
    free(value_after);
    free(value_before);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
    amxc_var_clean(&data);
}
