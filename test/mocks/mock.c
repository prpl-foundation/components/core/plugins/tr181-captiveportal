/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <netmodel/common_api.h>

#include "mock.h"
#include "common.h"
#include "captive_portal.h"

static netmodel_callback_t wan_intf_up = NULL;
static netmodel_callback_t lan_intf_ip_changed = NULL;

netmodel_callback_t test_netmodel_wan_intf_up_callback_get(void) {
    return wan_intf_up;
}

netmodel_callback_t test_netmodel_lan_intf_ip_callback_get(void) {
    return lan_intf_ip_changed;
}

netmodel_query_t* __wrap_netmodel_openQuery_isUp(UNUSED const char* intf,
                                                 UNUSED const char* subscriber,
                                                 UNUSED const char* flag,
                                                 UNUSED const char* traverse,
                                                 netmodel_callback_t handler,
                                                 UNUSED void* userdata) {
    netmodel_query_t* query = NULL;
    int rv = mock_type(int);
    if(rv == 0) {
        query = (netmodel_query_t*) calloc(1, 100);
        wan_intf_up = handler;
    }

    return query;
}

netmodel_query_t* __wrap_netmodel_openQuery_luckyAddrAddress(UNUSED const char* intf,
                                                             UNUSED const char* subscriber,
                                                             UNUSED const char* flag,
                                                             UNUSED const char* traverse,
                                                             netmodel_callback_t handler,
                                                             UNUSED void* userdata) {
    netmodel_query_t* query = (netmodel_query_t*) calloc(1, 100);
    lan_intf_ip_changed = handler;
    return query;
}

void __wrap_netmodel_closeQuery(netmodel_query_t* query) {
    free(query);
}

bool __wrap_netmodel_isUp(UNUSED const char* intf, UNUSED const char* flag, UNUSED const char* traverse) {
    return mock_type(bool);
}

amxc_var_t* __wrap_netmodel_luckyAddr(UNUSED const char* intf, UNUSED const char* flag, UNUSED const char* traverse) {
    amxd_dm_t* dm = cp_get_dm();
    amxd_object_t* object = amxd_dm_findf(dm, "Device.IP.Interface.lan.IPv4Address.lan.");
    char* ip_addr = amxd_object_get_value(cstring_t, object, "IPAddress", NULL);
    amxc_var_t* result = NULL;

    amxc_var_new(&result);

    amxc_var_set_type(result, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, result, "Address", ip_addr);
    amxc_var_add_key(cstring_t, result, "NetDevName", "br-lan");
    free(ip_addr);
    return result;
}

char* __wrap_netmodel_luckyAddrAddress(UNUSED const char* intf, UNUSED const char* flag, UNUSED const char* traverse) {
    amxd_dm_t* dm = cp_get_dm();
    amxd_object_t* object = amxd_dm_findf(dm, "Device.IP.Interface.lan.IPv4Address.lan.");
    char* ip_addr = amxd_object_get_value(cstring_t, object, "IPAddress", NULL);
    return ip_addr;
}

int __wrap_amxp_proc_ctrl_start(amxp_proc_ctrl_t* proc, UNUSED uint32_t minutes, UNUSED amxc_var_t* settings) {
    int rv = mock_type(int);

    if(rv == 0) {
        proc->proc->is_running = true;
    }
    return rv;
}

int __wrap_amxp_subproc_kill(const amxp_subproc_t* const subproc, UNUSED const int sig) {
    int rv = mock_type(int);
    amxp_subproc_t* modifiable_subproc = (amxp_subproc_t*) subproc;

    if(rv == 0) {
        modifiable_subproc->is_running = false;
    }
    return rv;
}

int __wrap_amxp_subproc_wait(UNUSED amxp_subproc_t* subproc, UNUSED int timeout_msec) {
    return mock_type(int);
}
